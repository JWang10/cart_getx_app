import 'package:get/get.dart';
import 'package:shopee_getx_app/models/product.dart';

class ShoppingController extends GetxController {
  var products = List<Product>.empty(growable: true).obs;

  @override
  void onInit() {
    super.onInit();
    fetchProducts();
  }

  void fetchProducts() async {
    await Future.delayed(Duration(seconds: 1));
    var productResult = [
      Product(
          id: 1,
          price: 30,
          productName: "FirstProd",
          productImage: "abd",
          productDescription: "some description"),
      Product(
          id: 2,
          price: 40,
          productName: "SecondProd",
          productImage: "abd",
          productDescription: "some description"),
      Product(
          id: 3,
          price: 50,
          productName: "ThirdProd",
          productImage: "abd",
          productDescription: "some description"),
    ];
    products.value = productResult;
  }
}
