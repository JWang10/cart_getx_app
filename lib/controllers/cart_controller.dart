import 'package:get/get.dart';
import 'package:shopee_getx_app/models/product.dart';

class CartController extends GetxController {
  var carItems = List<Product>.empty(growable: true).obs;
  double get totalPrice => carItems.fold(0, (sum, item) => sum + item.price);
  int get totalItem => carItems.length;

  addToCart(Product product) {
    carItems.add(product);
  }
}
