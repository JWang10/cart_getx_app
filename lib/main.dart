import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopee_getx_app/shopping_binding.dart';

import 'package:shopee_getx_app/views/shopping_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: ShoppingPage(),
      getPages: [
        GetPage(
            name: "/home",
            page: () => ShoppingPage(),
            binding: ShoppingBinding()),
      ],
      initialRoute: "/home",
    );
  }
}
