import 'package:get/get.dart';
import 'package:shopee_getx_app/controllers/cart_controller.dart';

import 'controllers/shopping_controller.dart';

class ShoppingBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ShoppingController());
    Get.put(CartController());
  }
}
