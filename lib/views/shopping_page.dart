import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopee_getx_app/controllers/cart_controller.dart';
import 'package:shopee_getx_app/controllers/shopping_controller.dart';

class ShoppingPage extends StatelessWidget {
  final shoppingController = Get.find<ShoppingController>();
  final cartController = Get.find<CartController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: GetX<ShoppingController>(
                builder: (controller) {
                  return ListView.builder(
                    itemCount: controller.products.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(12),
                        child: Padding(
                          padding: EdgeInsets.all(12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${shoppingController.products[index].productName}",
                                        style: TextStyle(fontSize: 24),
                                      ),
                                      Text(
                                        "${shoppingController.products[index].productDescription}",
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    "\$ ${shoppingController.products[index].price}",
                                    style: TextStyle(fontSize: 24),
                                  ),
                                ],
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  cartController
                                      .addToCart(controller.products[index]);
                                },
                                child: Text("Add to Cart"),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
            Obx(
              () => Text(
                "Total amount: \$ ${cartController.totalPrice}",
                style: TextStyle(fontSize: 24, color: Colors.white),
              ),
            ),
            SizedBox(height: 100),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.amber,
        onPressed: () {},
        icon: Icon(Icons.add_shopping_cart_rounded, color: Colors.black),
        label: Obx(
          () => Text("${cartController.totalItem}",
              style: TextStyle(fontSize: 16, color: Colors.black)),
        ),
      ),
    );
  }
}
